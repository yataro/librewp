# LibreWP

LibreWP is a libre fork of WordPress.

## Why

There are many things I don't like about WordPress. These things mostly violate the privacy of the site and its users.

These include:

* Sending the administrator User-Agent and site address to WordPress servers, as well as some sensitive information (example: user count).
* Generating auth keys and salts on a remote WordPress server under certain circumstances.
* Gravatar can track where users have left comments or have an account by looking at the request's referrer header.
* Remote images in `wp-admin/about.php` can be exploited to identify the site administrator by the referrer header.

This fork tries to fix all of the above problems.
The WordPress.org API can be turned back on, just like Gravatar profile images, using two simple constants in `wp-config.php`.

## TODO

* Auto-update
* FOSS plugin and theme registry

See the [WordPress README](README.WORDPRESS.md) for original WordPress README.
